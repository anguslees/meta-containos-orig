# A number of recipes (godep, kubernetes) assume go >=1.6 for ./vendor/
PREFERRED_VERSION_go = "1.9"
PREFERRED_PROVIDER_go-cross = "go"
PREFERRED_VERSION_go-native = "1.9"
